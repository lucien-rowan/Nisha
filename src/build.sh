# Valid variables are:
# GOOS: windows, darwin, linux, freebsd, netbsd, openbsd, aix, plan9, solaris, js
# GOARCH: 386, amd64, arm, arm64, loong64, ppc64, ppc64le, mips, mipsle, mips64, mips64le, riscv64, s390x, wasm
GOOS=linux GOARCH=amd64 go build -o ../bin/Nisha_Linux_amd64/nisha nisha.go