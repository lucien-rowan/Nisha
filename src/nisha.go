package main

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"hash"
	"image/color"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func genChecksum(file, hashfunc string) string {
	var h hash.Hash
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	switch hashfunc {
	case "MD5":
		h = md5.New()
	case "SHA1":
		h = sha1.New()
	case "SHA256":
		h = sha256.New()
	case "SHA512":
		h = sha512.New()
	}
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	result := hex.EncodeToString(h.Sum(nil))
	return result
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

type Resource interface {
	Name() string
	Content() []byte
}

type StaticResource struct {
	StaticName    string
	StaticContent []byte
}

func (r *StaticResource) Name() string {
	return r.StaticName
}

func (r *StaticResource) Content() []byte {
	return r.StaticContent
}

func NewStaticResource(name string, content []byte) *StaticResource {
	return &StaticResource{
		StaticName:    name,
		StaticContent: content,
	}
}

func LoadResourceFromPath(path string) (Resource, error) {
	bytes, err := ioutil.ReadFile(filepath.Clean(path))
	if err != nil {
		return nil, err
	}
	name := filepath.Base(path)
	return NewStaticResource(name, bytes), nil
}

func main() {
	myApp := app.New()
	myWindow := myApp.NewWindow("Nisha")
	myWindow.Resize(fyne.NewSize(800, 400))
	r, _ := LoadResourceFromPath("nisha.png")
	myWindow.SetIcon(r)

	// Main menu
	fileMenu := fyne.NewMenu("File",
		fyne.NewMenuItem("Quit", func() { myApp.Quit() }),
	)

	helpMenu := fyne.NewMenu("Help",
		fyne.NewMenuItem("About", func() {
			dialog.ShowCustom("About", "Close", container.NewVBox(
				widget.NewLabel("Nisha"),
				widget.NewLabel("Version: v2022.10.0"),
				widget.NewLabel("Author: Nicolas Lucien"),
			), myWindow)
		}))
	mainMenu := fyne.NewMainMenu(
		fileMenu,
		helpMenu,
	)
	myWindow.SetMainMenu(mainMenu)

	// Define a welcome text centered
	text := canvas.NewText("Welcome to Nisha!", color.White)
	text.Alignment = fyne.TextAlignCenter

	fileToHashTxt := widget.NewLabel("File")
	fileToHashTxt.Alignment = fyne.TextAlignCenter
	fileToHashField := widget.NewEntry()

	// Define a "browse" button
	browseBtn := widget.NewButton("Browse", func() {
		fileBrowser := dialog.NewFileOpen(
			func(r fyne.URIReadCloser, _ error) {
				selectedFile := r.URI().String()
				filePath := strings.TrimPrefix(selectedFile, "file://")
				fileToHashField.SetText(string(filePath))
			}, myWindow)
		fileBrowser.Show()
	})

	/*fileToHashBox := container.NewHBox(
		fileToHashTxt,
		fileToHashField,
		browseBtn,
	)*/

	shaTypeTxt := widget.NewLabel("Hash algorithm")
	shaTypeTxt.Alignment = fyne.TextAlignCenter

	shaTypeCombo := widget.NewSelect([]string{"MD5", "SHA1", "SHA256", "SHA512"}, func(value string) {
		log.Println("Select set to", value)
	})

	entryResult := widget.NewEntry()
	entryResult.MultiLine = true
	entryResult.Disable()

	hashButton := widget.NewButton("Hash", func() {
		if fileToHashField.Text != "" {
			info, err := os.Stat(fileToHashField.Text)
			if os.IsNotExist(err) {
				dialog.ShowInformation("Info",
					"File doesn't exist!", myWindow)
			} else {
				if info.IsDir() {
					dialog.ShowInformation("Info",
						"File is a directory!", myWindow)
				} else {
					if shaTypeCombo.Selected == "MD5" {
						go func() {
							checksum := genChecksum(fileToHashField.Text, "MD5")
							entryResult.SetText(checksum)
						}()
					} else if shaTypeCombo.Selected == "SHA1" {
						go func() {
							checksum := genChecksum(fileToHashField.Text, "SHA1")
							entryResult.SetText(checksum)
						}()
					} else if shaTypeCombo.Selected == "SHA256" {
						go func() {
							checksum := genChecksum(fileToHashField.Text, "SHA256")
							entryResult.SetText(checksum)
						}()
					} else if shaTypeCombo.Selected == "SHA512" {
						go func() {
							checksum := genChecksum(fileToHashField.Text, "SHA512")
							entryResult.SetText(checksum)
						}()
					}
				}
			}
		} else {
			dialog.ShowInformation("Info",
				"Please select a file!", myWindow)
		}
	})

	writeHashBtn := widget.NewButton("Write hash to file", func() {
		if entryResult.Text != "" {
			info, err := os.Stat(fileToHashField.Text)
			var fileWithHash string
			if os.IsNotExist(err) {
				dialog.ShowInformation("Info",
					"File doesn't exist!", myWindow)
			} else {
				if info.IsDir() {
					dialog.ShowInformation("Info",
						"File is a directory!", myWindow)
				} else {
					if shaTypeCombo.Selected == "MD5" {
						fileWithHash = fileToHashField.Text + ".md5"
					} else if shaTypeCombo.Selected == "SHA1" {
						fileWithHash = fileToHashField.Text + ".sha1"
					} else if shaTypeCombo.Selected == "SHA256" {
						fileWithHash = fileToHashField.Text + ".sha256"
					} else if shaTypeCombo.Selected == "SHA512" {
						fileWithHash = fileToHashField.Text + ".sha512"
					}

					f, err := os.Create(fileWithHash)
					check(err)

					n3, err := f.WriteString(entryResult.Text)
					check(err)
					fmt.Println(n3)
				}
			}
		} else {
			dialog.ShowInformation("Info",
				"Please select a file and get the hash!", myWindow)
		}
	})

	// Display a vertical box containing text, image and button
	box := container.New(
		layout.NewGridLayout(3),
		fileToHashTxt, fileToHashField, browseBtn, shaTypeTxt, shaTypeCombo, hashButton, entryResult, writeHashBtn,
	)

	// Display our content
	myWindow.SetContent(box)

	// Close the App when Escape key is pressed
	myWindow.Canvas().SetOnTypedKey(func(keyEvent *fyne.KeyEvent) {

		if keyEvent.Name == fyne.KeyEscape {
			myApp.Quit()
		}
	})

	// Show window and run app
	myWindow.ShowAndRun()
}
