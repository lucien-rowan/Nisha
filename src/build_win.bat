Rem Valid variables are:
Rem GOOS: windows, darwin, linux, freebsd, netbsd, openbsd, aix, plan9, solaris, js
Rem GOARCH: 386, amd64, arm, arm64, loong64, ppc64, ppc64le, mips, mipsle, mips64, mips64le, riscv64, s390x, wasm

set GOOS=windows
set GOARCH=amd64
go build -o ../bin/Nisha_Win_amd64/nisha.exe nisha.go